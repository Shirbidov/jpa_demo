package az.shirbidov.JPA.repo;

import az.shirbidov.JPA.domain.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {

    List<EmployeeEntity> findBySurnameLike(String surname);
    List<EmployeeEntity> findByNameOrSurname(String name,String surname);
    List<EmployeeEntity> findByIdLessThan(Long id);


}
