package az.shirbidov.JPA;

import az.shirbidov.JPA.domain.EmployeeEntity;
import az.shirbidov.JPA.repo.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class JpaApplication implements CommandLineRunner {

    final PageRequest pageRequest = PageRequest.of(0,3, Sort.Direction.ASC,"name");

    final private EmployeeRepository employeeRepository;

    public static void main(String[] args) {
        SpringApplication.run(JpaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        employeeRepository
//                .findBySurnameLike("S%")
//                .stream()
//                .forEach(System.out::println);
//        employeeRepository.findByIdLessThan(2L)
//                .stream().
//                forEach(System.out::println);
//        System.out.println(employeeRepository.findByNameOrSurname("Ruslan", "Shirbidov"));

        employeeRepository.findAll(pageRequest);
    }

}
