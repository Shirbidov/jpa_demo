package az.shirbidov.JPA.rest;

import az.shirbidov.JPA.domain.EmployeeEntity;
import az.shirbidov.JPA.repo.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/emp")
@RequiredArgsConstructor
public class EmployeeController {
    final EmployeeRepository employeeRepository;
    final PageRequest pageRequest = PageRequest.of(0,3, Sort.Direction.ASC,"name");


    @GetMapping
    public Page<EmployeeEntity> getEmployeeNameByID(EmployeeEntity requiredEmployee){

        Page<EmployeeEntity> entity = employeeRepository.findAll(pageRequest);
        return entity;
    }


}
